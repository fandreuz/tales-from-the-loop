#!/bin/bash
###############################################################################
# Description   : Push a list of branches. Useful in conjunction with
#                    merge-many.sh
# Author        : fandreuz
# Usage         : ./push-many.sh branch.txt
###############################################################################

if [ $# -lt 2 ]; then
    CI_BRANCHES_FILE="/dev/null"
else
    CI_BRANCHES_FILE=$2
fi

SKIP_CI="--push-option=ci.skip"

set -ex
while IFS= read -r BRANCH_NAME
do
    echo "Checking out $BRANCH_NAME"
    git checkout $BRANCH_NAME
    echo "Pushing $BRANCH_NAME"
    if [[ $(grep "$BRANCH_NAME" $CI_BRANCHES_FILE) ]]; then
        git push origin $BRANCH_NAME
    else
        git push origin $BRANCH_NAME $SKIP_CI
    fi
done < "$1"
set +ex
