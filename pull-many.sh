#!/bin/bash
###############################################################################
# Description   : Push a list of branches. Useful in conjunction with 
#                    merge-many.sh
# Author        : fandreuz
# Usage         : ./push-many.sh branch.txt
###############################################################################

set -e
while IFS= read -r BRANCH_NAME
do
    echo "Checking out $BRANCH_NAME"
    git checkout $BRANCH_NAME
    echo "Pulling $BRANCH_NAME"
    git pull origin $BRANCH_NAME
done < "$1"
set +e