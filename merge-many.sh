#!/bin/bash
###############################################################################
# Description   : Merge iteratively a list of branches.
# Author        : fandreuz
# Usage         : ./merge-many.sh branch.txt
###############################################################################

PREV=""
set -e
while IFS= read -r BRANCH_NAME
do
    echo "Checking out $BRANCH_NAME"
    git checkout $BRANCH_NAME
    if [[ $PREV ]]; then
        echo "Merging $PREV into $BRANCH_NAME"
        GIT_EDITOR=true git merge $PREV
    fi
    PREV=$BRANCH_NAME
done < "$1"
set +e
