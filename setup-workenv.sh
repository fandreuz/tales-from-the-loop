#!/usr/bin/env bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
# FLAGS="ssh  -o StrictHostKeyChecking=no"
PROXY=lxplus.cern.ch
set -x

# When host names are passed to sshuttle, the script tries to resolve them to IP addresses via Python's socket.getaddrinfo.
# This means that the domain names must be resolvable *before* the tunneling is active, or an exception is raised. This happens
# even when the "--dns" flag is provided, because the resolution happens as part of the parsing of CLI arguments.
# A poor man solution is to resolve IP manually on an host on which those domain addresses can be resolved, such that
# sshuttle does not neeed to do anything in this sense.
IPS=$(ssh lxplus.cern.ch "getent hosts $(cat $SCRIPT_DIR/hosts.txt | tr '\n' ' ' | envsubst)" | awk '{print $1}' | tr '\n' ' ')

sshuttle \
   -r $(whoami)@$PROXY \
   -x $PROXY:22 \
   --dns \
   $IPS
